<?php

// If uninstall is not called from WordPress or access directly then, exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}
// exit if current user does not have permission to delete plugin
if ( ! current_user_can( 'delete_plugins' ) )
    wp_die('You do not have permission to delete plugins.');


// get all posts added by the plugin
$ps_args = array(
    'numberposts' => -1,
    'post_type'   => 'psgalleryblock',
    'post_status' => 'any',
);

$ps_sliders = get_posts( $ps_args );
foreach ($ps_sliders as $slider){
    // delete all meta data related to the slider
    delete_post_meta($slider->ID, 'ps_display_header');
    delete_post_meta($slider->ID, 'ps_select_theme');
    delete_post_meta($slider->ID, 'ps_display_navigation_arrows');
    delete_post_meta($slider->ID, 'ps_title');
    delete_post_meta($slider->ID, 'ps_total_posts');
    delete_post_meta($slider->ID, 'ps_posts_type');
    delete_post_meta($slider->ID, 'ps_posts_bycategory');
    delete_post_meta($slider->ID, 'ps_posts_byID');
    delete_post_meta($slider->ID, 'ps_posts_byTag');
    delete_post_meta($slider->ID, 'ps_image_crop');
    delete_post_meta($slider->ID, 'ps_crop_image_height');
    delete_post_meta($slider->ID, 'ps_crop_image_width');
    delete_post_meta($slider->ID, 'ps_display_post_title');


    delete_post_meta($slider->ID, 'ps_display_post_date');
    delete_post_meta($slider->ID, 'ps_auto_play');
    delete_post_meta($slider->ID, 'ps_stop_on_hover');
    delete_post_meta($slider->ID, 'ps_slide_speed');
    delete_post_meta($slider->ID, 'ps_item_on_desktop');
    delete_post_meta($slider->ID, 'ps_item_on_tablet');
    delete_post_meta($slider->ID, 'ps_item_on_mobile');
    delete_post_meta($slider->ID, 'ps_pagination');
    delete_post_meta($slider->ID, 'ps_display_excerpt');
    delete_post_meta($slider->ID, 'ps_excerpt_length');

    delete_post_meta($slider->ID, 'ps_display_placeholder_img');
    delete_post_meta($slider->ID, 'ps_default_feat_img');
    delete_post_meta($slider->ID, 'ps_display_img');
    delete_post_meta($slider->ID, 'ps_header_title_font_size');
    delete_post_meta($slider->ID, 'ps_header_title_font_color');
    delete_post_meta($slider->ID, 'ps_nav_arrow_color');
    delete_post_meta($slider->ID, 'ps_nav_arrow_bg_color');
    delete_post_meta($slider->ID, 'ps_nav_arrow_hover_color');
    delete_post_meta($slider->ID, 'ps_nav_arrow_bg_hover_color');
    delete_post_meta($slider->ID, 'ps_border_color');
    delete_post_meta($slider->ID, 'ps_border_hover_color');
    delete_post_meta($slider->ID, 'ps_title_font_size');
    delete_post_meta($slider->ID, 'ps_title_font_color');
    delete_post_meta($slider->ID, 'ps_title_hover_font_color');

    wp_delete_post( $slider->ID, true); // delete each post
}

