<?php

/**
 * Plugin Name: PS Gallery Plugin
 * Description: A gallery plugin form pranon studio
 * Version: 1.0.0
 * Domain Path: /languages
 * Text Domain: ps-gallery-block
 * Author: Pranon Studio
 * Author URI: https://pranon.com
 * License: A "GPL2"
 **/

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

// check for required php version and deactivate the plugin if php version is less.
if (version_compare(PHP_VERSION, '5.4', '<')) {
    add_action('admin_notices', 'ps_notice');
    function ps_notice()
    { ?>
        <div class="error notice is-dismissible"><p>
                <?php
                echo 'This requires minimum PHP 5.4 to function properly. Please upgrade PHP version. The Plugin has been auto-deactivated.. You have PHP version ' . PHP_VERSION;
                ?>
            </p></div>
        <?php
        if (isset($_GET['activate'])) {
            unset($_GET['activate']);
        }
    }

    // deactivate the plugin because required php version is less.
    add_action('admin_init', 'ps_deactivate_self');
    function ps_deactivate_self()
    {
        deactivate_plugins(plugin_basename(__FILE__));
    }

    return;
}

/*
 * All Constants
 */
if (!defined('PS_VERSION')) define('PS_VERSION', '1.0.0');
if (!defined('PS_BASENAME')) define('PS_BASENAME', plugin_basename(__FILE__));
if (!defined('PS_MINIMUM_WP_VERSION')) define('PS_MINIMUM_WP_VERSION', '3.5');
if (!defined('PS_PLUGIN_DIR')) define('PS_PLUGIN_DIR', plugin_dir_path(__FILE__));
if (!defined('PS_PLUGIN_URI')) define('PS_PLUGIN_URI', plugins_url('', __FILE__));
if (!defined('PS_TEXTDOMAIN')) define('PS_TEXTDOMAIN', 'ps-gallery-block');
if (!defined('PS_DEFAULT_IMG')) define('PS_DEFAULT_IMG', PS_PLUGIN_URI . '/img/featured_image_placeholder.jpg');
if (!defined('PS_ALERT_MSG')) define('PS_ALERT_MSG', __('Sorry! This is not your place!', PS_TEXTDOMAIN));

// All includes

require_once PS_PLUGIN_DIR . 'includes/ps-helper.php';
require_once PS_PLUGIN_DIR . 'includes/ps-register-post.php';
require_once PS_PLUGIN_DIR . 'includes/ps-cpt-columns.php';
require_once PS_PLUGIN_DIR . 'includes/ps-featured-post.php';
require_once PS_PLUGIN_DIR . 'includes/ps-popular-post.php';
require_once PS_PLUGIN_DIR . 'includes/ps-image-resizer.php';
require_once PS_PLUGIN_DIR . 'includes/ps-metabox.php';
require_once PS_PLUGIN_DIR . 'includes/ps-metabox-save.php';
require_once PS_PLUGIN_DIR . 'includes/ps-shortcode.php';
require_once PS_PLUGIN_DIR . 'includes/upgrade-support.php';

// warn if unsupported WordPress Version. This function should be called after including helper.php
if (ps_check_min_wp_version(PS_MINIMUM_WP_VERSION)) {
    add_action('admin_notices', 'ps_warn_if_unsupported_wp');
    add_action('admin_init', 'ps_deactivate_self');
    function ps_deactivate_self()
    {
        deactivate_plugins(PS_BASENAME);
    }
}
// Registering all styles and scripts
function ps_enqueue_styles()
{
    //styles
    wp_register_style('owl-carousel-min-style', PS_PLUGIN_URI . '/css/owl.carousel.min.css', false, PS_VERSION);
    wp_register_style('ps-gallery-block-frontend', PS_PLUGIN_URI . '/css/ps-gallery-block-frontend.css', false, PS_VERSION);
    wp_register_style('material-design-iconic-font', PS_PLUGIN_URI . '/css/material-design-iconic-font.min.css', false, PS_VERSION);
    wp_register_style('owl-theme-default-min-style', PS_PLUGIN_URI . '/css/owl.theme.default.min.css', array('owl-carousel-min-style'), PS_VERSION);

    //scripts
    wp_register_script('owl-carousel-min-script', PS_PLUGIN_URI . '/js/owl.carousel.min.js', array('jquery'), PS_VERSION, true);
    wp_register_script('ps-front-end-script', PS_PLUGIN_URI . '/js/ps-front-end.js', array('jquery'), PS_VERSION, true);
}

add_action('wp_enqueue_scripts', 'ps_enqueue_styles');
function ps_enqueue_admin_scripts_and_styles()
{
    global $typenow;
    if ('psgalleryblock' === $typenow) {
        wp_enqueue_style('cmb2-style', PS_PLUGIN_URI . '/css/cmb2.min.css', false, PS_VERSION);
        wp_enqueue_script('admin-script', PS_PLUGIN_URI . '/js/ps-admin.js', array('jquery', 'wp-color-picker'), PS_VERSION, true);
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_media();
    }
    if ('psgalleryblock' === $typenow || 'post' === $typenow) {
        wp_enqueue_style('admin-style', PS_PLUGIN_URI . '/css/ps-admin.css', false, PS_VERSION);
    }

    wp_enqueue_style('material-design-iconic-font');

}

add_action('admin_enqueue_scripts', 'ps_enqueue_admin_scripts_and_styles');


