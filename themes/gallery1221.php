<div class="ps-gallery-block-inner post-<?php the_ID(); ?>">
    <div class="ps-gallery-block-content">
        <?php $categories = get_the_category();

        if (!empty($categories)) { ?>
            <div class="ps-entry-category">
                <?php echo '<a class="ps-cat-tag"' . ' href="' . esc_url(get_category_link($categories[0]->term_id)) . '">' . esc_html($categories[0]->name) . '</a>'; ?>
            </div>
        <?php } ?>

        <div class="gallery-block-entry-header">

            <?php if ($ps_display_post_title === 'yes' || $ps_display_excerpt === 'yes') {
                $basecol = $col[$i];
                $searchword = 'ps-gallery-block-large';
                if (preg_match("/\b$searchword\b/i", $basecol)) { ?>
                    <?php if ($ps_display_post_title === 'yes') { ?>
                        <h2 class="ps-entry-title">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"
                               title="Go to <?php the_title_attribute(); ?>"><?php echo wp_trim_words(get_the_title(), $ps_title_excerpt_length, ''); ?></a>
                        </h2>
                    <?php } ?>

                    <?php if ($ps_display_excerpt === 'yes') { ?>
                        <div class="ps-entry-content">
                            <?php echo ps_get_limited_excerpts($ps_excerpt_length); ?>
                        </div>
                    <?php } ?>


                <?php } else { ?>

                    <?php if ($ps_display_post_title === 'yes') { ?>
                        <h2 class="ps-entry-title">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"
                               title="Go to <?php the_title_attribute(); ?>"><?php echo wp_trim_words(get_the_title(), $ps_small_title_excerpt_length, ''); ?></a>
                        </h2>
                    <?php } ?>

                    <?php if ($ps_display_excerpt === 'yes') { ?>
                        <div class="ps-entry-content">
                            <?php echo ps_get_limited_excerpts($ps_small_excerpt_length); ?>
                        </div>
                    <?php } ?>

                <?php }
            } ?>


            <div class="entry-meta clearfix">

                <?php if ($ps_display_post_date === 'yes') { ?>
                    <div class="ps-meta-info">
                        <span class="ps-entry-date entry-date pull-left">
                           <i class="zmdi zmdi-time-countdown"></i><?php echo get_the_date(); ?>
                         </span>
                    </div>
                <?php } ?>

                <span class="pull-right ps-gallery-comment">
                <i class="zmdi zmdi-comment-outline"></i>
                    <?php comments_number('0', '1', '%'); ?>
            </span>

            </div>
        </div>

        <?php
        // show image if it is allowed
        if ('yes' === $ps_display_img) {
            // get featured image, if not, get first image, if not get default image
            $image_url = '';
            if (has_post_thumbnail()) {
                //$image_url  =  get_the_post_thumbnail_url();
                $thumb = get_post_thumbnail_id();
                $image_url = wp_get_attachment_url($thumb, 'full'); //get img URL, and this function recieve only 1 arg. wp-includes/posts.php @5005 line
            } else {
                $image_url = $ps_default_feat_img; //Get Placeholder Image From Settings
            } // ends has_post_thumbnail() condition

            // crop the image if it is enabled
            if (!empty($image_url) && !empty($ps_image_crop) && $ps_image_crop === 'yes') {
                $image_url = aq_resize($image_url, $ps_crop_image_width, $ps_crop_image_height, true, true, true); //resize & crop img

            }
            // show the image if image found
            if (!empty($image_url)) { ?>

                <div class="ps-gallery-block-bg">
                    <div class="ps-post-image"
                         style="background-image: url('<?php echo esc_url($image_url) ?>');"></div>
                </div>

            <?php } // ends !empty($image_url)

        }  // ends show image if it is allowed
        ?>

    </div>
</div>