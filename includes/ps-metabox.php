<?php

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

function ps_add_meta_box()
{
    add_meta_box(
        'ps_metabox',
        __('Gallery Block Settings & Shortcode Generator', PS_TEXTDOMAIN),
        'ps_metabox_cb',
        'psgalleryblock',
        'normal'
    );
}

add_action('add_meta_boxes', 'ps_add_meta_box');


/**
 * Display metabox content
 * @param Object $post | The current post object.
 */
function ps_metabox_cb($post)
{


    // Add a nonce field so we can check for it later.
    wp_nonce_field('ps_meta_save', 'ps_meta_save_nounce');

    // temp vars
    $ps_display_header = get_post_meta($post->ID, 'ps_display_header', true);
    $ps_select_theme = get_post_meta($post->ID, 'ps_select_theme', true);
    $ps_display_navigation_arrows = get_post_meta($post->ID, 'ps_display_navigation_arrows', true);
    $ps_title = get_post_meta($post->ID, 'ps_title', true);
    $ps_total_posts = get_post_meta($post->ID, 'ps_total_posts', true);
    //query type
    $ps_posts_type = get_post_meta($post->ID, 'ps_posts_type', true);
    $ps_posts_bycategory = get_post_meta($post->ID, 'ps_posts_bycategory', true);
    $ps_posts_byID = get_post_meta($post->ID, 'ps_posts_byID', true);
    $ps_posts_byTag = get_post_meta($post->ID, 'ps_posts_byTag', true);


    $ps_display_placeholder_img = get_post_meta($post->ID, 'ps_display_placeholder_img', true);
    $ps_default_feat_img = get_post_meta($post->ID, 'ps_default_feat_img', true);
    $ps_display_img = get_post_meta($post->ID, 'ps_display_img', true);


    $ps_image_crop = get_post_meta($post->ID, 'ps_image_crop', true);
    $ps_crop_image_width = get_post_meta($post->ID, 'ps_crop_image_width', true);
    $ps_crop_image_height = get_post_meta($post->ID, 'ps_crop_image_height', true);
    $ps_display_post_title = get_post_meta($post->ID, 'ps_display_post_title', true);
    $ps_display_post_date = get_post_meta($post->ID, 'ps_display_post_date', true);

    $ps_display_excerpt = get_post_meta($post->ID, 'ps_display_excerpt', true);
    $ps_excerpt_length = get_post_meta($post->ID, 'ps_excerpt_length', true);
    $ps_small_excerpt_length = get_post_meta($post->ID, 'ps_small_excerpt_length', true);

    $ps_auto_play = get_post_meta($post->ID, 'ps_auto_play', true);
    $ps_stop_on_hover = get_post_meta($post->ID, 'ps_stop_on_hover', true);
    $ps_slide_speed = get_post_meta($post->ID, 'ps_slide_speed', true);
    $ps_item_on_desktop = get_post_meta($post->ID, 'ps_item_on_desktop', true);
    $ps_item_on_tablet = get_post_meta($post->ID, 'ps_item_on_tablet', true);
    $ps_item_on_mobile = get_post_meta($post->ID, 'ps_item_on_mobile', true);
    $ps_pagination = get_post_meta($post->ID, 'ps_pagination', true);

    $ps_header_title_font_size = get_post_meta($post->ID, 'ps_header_title_font_size', true);
    $ps_header_title_font_color = get_post_meta($post->ID, 'ps_header_title_font_color', true);
    $ps_nav_arrow_color = get_post_meta($post->ID, 'ps_nav_arrow_color', true);
    $ps_nav_arrow_bg_color = get_post_meta($post->ID, 'ps_nav_arrow_bg_color', true);
    $ps_nav_arrow_hover_color = get_post_meta($post->ID, 'ps_nav_arrow_hover_color', true);
    $ps_nav_arrow_bg_hover_color = get_post_meta($post->ID, 'ps_nav_arrow_bg_hover_color', true);

    $ps_border_color = get_post_meta($post->ID, 'ps_border_color', true);
    $ps_border_hover_color = get_post_meta($post->ID, 'ps_border_hover_color', true);

    $ps_title_font_size = get_post_meta($post->ID, 'ps_title_font_size', true);
    $ps_title_font_color = get_post_meta($post->ID, 'ps_title_font_color', true);
    $ps_title_hover_font_color = get_post_meta($post->ID, 'ps_title_hover_font_color', true);


    // sanitaized vars

    $ps_display_header = (!empty($ps_display_header)) ? esc_attr($ps_display_header) : '';
    $ps_select_theme = (!empty($ps_select_theme)) ? esc_attr($ps_select_theme) : '';
    $ps_display_navigation_arrows = (!empty($ps_display_navigation_arrows)) ? esc_attr($ps_display_navigation_arrows) : '';
    $ps_title = (!empty($ps_title)) ? esc_attr($ps_title) : '';
    $ps_total_posts = (!empty($ps_total_posts)) ? esc_attr($ps_total_posts) : '';
    //query type
    $ps_posts_type = (!empty($ps_posts_type)) ? esc_attr($ps_posts_type) : '';
    $ps_posts_bycategory = (!empty($ps_posts_bycategory)) ? esc_attr($ps_posts_bycategory) : '';
    $ps_posts_byID = (!empty($ps_posts_byID)) ? esc_attr($ps_posts_byID) : '';
    $ps_posts_byTag = (!empty($ps_posts_byTag)) ? esc_attr($ps_posts_byTag) : '';

    $ps_display_placeholder_img = (!empty($ps_display_placeholder_img)) ? esc_attr($ps_display_placeholder_img) : '';
    $ps_default_feat_img = (!empty($ps_default_feat_img)) ? esc_attr($ps_default_feat_img) : '';
    $ps_display_img = (!empty($ps_display_img)) ? esc_attr($ps_display_img) : '';
    $ps_image_crop = (!empty($ps_image_crop)) ? esc_attr($ps_image_crop) : '';
    $ps_crop_image_width = (!empty($ps_crop_image_width)) ? esc_attr($ps_crop_image_width) : '';
    $ps_crop_image_height = (!empty($ps_crop_image_height)) ? esc_attr($ps_crop_image_height) : '';
    $ps_auto_play = (!empty($ps_auto_play)) ? esc_attr($ps_auto_play) : '';
    $ps_stop_on_hover = (!empty($ps_stop_on_hover)) ? esc_attr($ps_stop_on_hover) : '';
    $ps_slide_speed = (!empty($ps_slide_speed)) ? esc_attr($ps_slide_speed) : 5000;
    $ps_item_on_desktop = (!empty($ps_item_on_desktop)) ? absint(esc_attr($ps_item_on_desktop)) : 4;
    $ps_item_on_tablet = (!empty($ps_item_on_tablet)) ? absint(esc_attr($ps_item_on_tablet)) : 3;
    $ps_item_on_mobile = (!empty($ps_item_on_mobile)) ? absint(esc_attr($ps_item_on_mobile)) : 2;
    $ps_pagination = (!empty($ps_pagination)) ? esc_attr($ps_pagination) : '';


    $ps_header_title_font_size = (!empty($ps_header_title_font_size)) ? esc_attr($ps_header_title_font_size) : '';
    $ps_header_title_font_color = (!empty($ps_header_title_font_color)) ? esc_attr($ps_header_title_font_color) : '';
    $ps_nav_arrow_color = (!empty($ps_nav_arrow_color)) ? esc_attr($ps_nav_arrow_color) : '';
    $ps_nav_arrow_bg_color = (!empty($ps_nav_arrow_bg_color)) ? esc_attr($ps_nav_arrow_bg_color) : '';
    $ps_nav_arrow_hover_color = (!empty($ps_nav_arrow_hover_color)) ? esc_attr($ps_nav_arrow_hover_color) : '';
    $ps_nav_arrow_bg_hover_color = (!empty($ps_nav_arrow_bg_hover_color)) ? esc_attr($ps_nav_arrow_bg_hover_color) : '';
    $ps_border_color = (!empty($ps_border_color)) ? esc_attr($ps_border_color) : '';
    $ps_border_hover_color = (!empty($ps_border_hover_color)) ? esc_attr($ps_border_hover_color) : '';
    $ps_title_font_size = (!empty($ps_title_font_size)) ? esc_attr($ps_title_font_size) : '';
    $ps_title_font_color = (!empty($ps_title_font_color)) ? esc_attr($ps_title_font_color) : '';
    $ps_title_hover_font_color = (!empty($ps_title_hover_font_color)) ? esc_attr($ps_title_hover_font_color) : '';


    ?>
    <div id="tabs-container">

        <ul class="tabs-menu">
            <li class="current"><a href="#tab-1"><?php _e('General Settings', PS_TEXTDOMAIN); ?></a></li>
            <li><a href="#tab-2"><?php _e('Slider Settings', PS_TEXTDOMAIN); ?></a></li>
            <li><a href="#tab-3"><?php _e('Style Settings', PS_TEXTDOMAIN); ?></a></li>
        </ul>

        <div class="tab">

            <div id="tab-1" class="tab-content">
                <div class="cmb2-wrap form-table">
                    <div id="cmb2-metabox" class="cmb2-metabox cmb-field-list">

                        <!--Display Header ? -->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label for="ps_display_header"><?php _e('Display Header', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_display_header" id="ps_display_header"
                                                   value="yes" <?php if ($ps_display_header != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>

                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Display Gallery Blog Title', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-select">
                            <div class="cmb-th">
                                <label for="ps_select_theme"><?php _e('Select a Theme', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list radio-image-inline-list">
                                    <li class="img-radiobox">
                                        <input type="radio" class="cmb2-option" name="ps_select_theme" id="gallery_default"
                                               value="gallerydefault" <?php if ($ps_select_theme == 'gallerydefault'){
                                            echo "checked";
                                        } else {
                                            echo "checked";
                                        }; ?>>
                                        <label class="drinkcard-cc gallery_default_img" for="gallery_default"></label>
                                    </li>

                                    <li class="img-radiobox">
                                        <input type="radio" class="cmb2-option" name="ps_select_theme" id="gallery_2x3"
                                               value="gallery23" <?php checked($ps_select_theme, 'gallery23'); ?>>
                                        <label class="drinkcard-cc gallery_2x3_img" for="gallery_2x3"></label>
                                    </li>

                                    <li class="img-radiobox">
                                        <input type="radio" class="cmb2-option" name="ps_select_theme" id="gallery_212"
                                               value="gallery212" <?php checked($ps_select_theme, 'gallery212'); ?>>
                                        <label class="drinkcard-cc gallery_212_img" for="gallery_212"></label>
                                    </li>

                                    <li class="img-radiobox">
                                        <input type="radio" class="cmb2-option" name="ps_select_theme" id="gallery_1221"
                                               value="gallery1221" <?php checked($ps_select_theme, 'gallery1221'); ?>>
                                        <label class="drinkcard-cc gallery_1221_img" for="gallery_1221"></label>
                                    </li>

                                    <li class="img-radiobox">
                                        <input type="radio" class="cmb2-option" name="ps_select_theme" id="gallery_122"
                                               value="gallery122" <?php checked($ps_select_theme, 'gallery122'); ?>>
                                        <label class="drinkcard-cc gallery_122_img" for="gallery_122"></label>
                                    </li>

                                    <li class="img-radiobox">
                                        <input type="radio" class="cmb2-option" name="ps_select_theme" id="gallery_carousel"
                                               value="gallerycarousel" <?php checked($ps_select_theme, 'gallerycarousel'); ?>>
                                        <label class="drinkcard-cc gallery_carousel_img" for="gallery_carousel"></label>
                                    </li>


                                </ul>

                            </div>
                        </div>

                        <!--Title Above Slider-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_title"><?php _e('Title Above Slider', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-medium" name="ps_title" id="ps_title"
                                       value="<?php if (empty($ps_title)) {
                                           _e('Latest Posts', PS_TEXTDOMAIN);
                                       } else {
                                           echo $ps_title;
                                       } ?>">
                                <p class="cmb2-metabox-description"><?php _e('Slider title', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <!--Total posts to display -->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_total_posts"><?php _e('Total Posts', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_total_posts" id="ps_total_posts"
                                       value="<?php if (empty($ps_total_posts)) {
                                           echo 12;
                                       } else {
                                           echo $ps_total_posts;
                                       } ?>">
                                <p class="cmb2-metabox-description"><?php _e('How many posts to display in the slider', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>

                        <div class="cmb-row cmb-type-multicheck">
                            <div class="cmb-th">
                                <label
                                    for="ps_posts_type"><?php _e('Posts Query Type', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><input type="radio" class="cmb2-option"
                                               name="ps_posts_type"
                                               id="ps_posts_type1"
                                               value="latest" <?php if ($ps_posts_type == "latest") {
                                            echo "checked";
                                        } else {
                                            echo "checked";
                                        } ?>> <label
                                            for="ps_posts_type1"><?php _e('Latest Posts', PS_TEXTDOMAIN); ?></label>
                                    </li>

                                    <li><input type="radio" class="cmb2-option"
                                               name="ps_posts_type"
                                               id="ps_posts_type2"
                                               value="older" <?php if ($ps_posts_type == "older") {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        } ?>> <label
                                            for="ps_posts_type2"><?php _e('Older Posts', PS_TEXTDOMAIN); ?></label>
                                    </li>

                                    <li><input type="radio" class="cmb2-option"
                                               name="ps_posts_type"
                                               id="ps_posts_type3"
                                               value="featured" <?php if ($ps_posts_type == "featured") {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        } ?>> <label
                                            for="ps_posts_type3"><?php _e('Featured Posts', PS_TEXTDOMAIN); ?></label>
                                    </li>

                                    <li><input type="radio" class="cmb2-option"
                                               name="ps_posts_type"
                                               id="ps_posts_type9"
                                               value="popular_post" <?php if ($ps_posts_type == "popular_post") {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        } ?>> <label
                                            for="ps_posts_type9"><?php _e('Popular Posts', PS_TEXTDOMAIN); ?></label>
                                    </li>

                                    <li><input type="radio"
                                               class="cmb2-option"
                                               name="ps_posts_type"
                                               id="ps_posts_type4"
                                               value="category" <?php if ($ps_posts_type == "category") {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        } ?>> <label class=""
                                                     for="ps_posts_type4"><?php _e('Posts by Category', PS_TEXTDOMAIN); ?></label>
                                    </li>
                                    <input type="text"
                                           class="cmb2-text-medium"
                                           name="ps_posts_bycategory"
                                           id="ps_posts_bycategory"
                                           value="<?php if (!empty($ps_posts_bycategory)) {
                                               echo $ps_posts_bycategory;
                                           } else {
                                               echo '';
                                           } ?>"
                                           placeholder="e.g. wordpress, php, news">

                                    <li class="postsbyidw"><input
                                            type="radio"
                                            class="cmb2-option"
                                            name="ps_posts_type"
                                            id="ps_posts_type5"
                                            value="postsbyid" <?php if ($ps_posts_type == "postsbyid") {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        } ?>> <label class=""
                                                     for="ps_posts_type5"><?php _e('Posts by ID', PS_TEXTDOMAIN); ?></label>
                                    </li>
                                    <input type="text"
                                           class="cmb2-text-medium"
                                           name="ps_posts_byID" id="ps_posts_byID"
                                           value="<?php if (!empty($ps_posts_byID)) {
                                               echo $ps_posts_byID;
                                           } else {
                                               echo '';
                                           } ?>" placeholder="e.g. 1, 5, 10">


                                    <li><input type="radio"
                                               class="cmb2-option"
                                               name="ps_posts_type"
                                               id="ps_posts_type6"
                                               value="postsbytag" <?php if ($ps_posts_type == "postsbytag") {
                                            echo "checked";
                                        } else {
                                            echo "";
                                        } ?>> <label class=""
                                                     for="ps_posts_type6"><?php _e('Posts by Tags', PS_TEXTDOMAIN); ?></label>
                                    </li>
                                    <input type="text"
                                           class="cmb2-text-medium"
                                           name="ps_posts_byTag"
                                           id="ps_posts_byTag"
                                           value="<?php if (!empty($ps_posts_byTag)) {
                                               echo $ps_posts_byTag;
                                           } else {
                                               echo '';
                                           } ?>"
                                           placeholder="e.g. food, tree, water">
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Select how you like to display post', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>


                        <!--Show featured image -->

                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label for="ps_display_img"><?php _e('Show featured image', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_display_img" id="ps_display_img"
                                                   value="yes" <?php if ($ps_display_img != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Display featured image of the post. If featured image is not found then the first image from the post content will be used.', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>

                        <!--Show featured image placeholder-->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label
                                    for="ps_display_placeholder_img"><?php _e('Use Placeholder image', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_display_placeholder_img"
                                                   id="ps_display_placeholder_img"
                                                   value="yes" <?php if ($ps_display_placeholder_img != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Display a featured image placeholder if a post has no featured image ?', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>

                        <!--Upload featured image placeholder-->

                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label
                                    for="ps_default_feat_img"><?php _e('Upload Placeholder image', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li>
                                        <input type="text" name="ps_default_feat_img" id="ps_default_feat_img"
                                               class="regular-text"
                                               value="<?php echo (!empty($ps_default_feat_img)) ? $ps_default_feat_img : PS_DEFAULT_IMG; ?>">
                                        <input type="button" name="upload-btn" id="upload-btn" class="button-secondary"
                                               value="Upload Image">
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Upload a featured image placeholder. Otherwise, plugin\'s default image will be used', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>


                        <!--Crop image ? -->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label for="ps_image_crop"><?php _e('Auto Crop and Resize Image', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_image_crop" id="ps_image_crop"
                                                   value="yes" <?php if ($ps_image_crop != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Enable cropping and resizing image automatically. If you use this feature, then you can not use default placeholder that comes with this plugin. You need to upload your own default placeholder image if you want to use.', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>

                        <!--Image Widht-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_crop_image_width"><?php _e('Image Width', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_crop_image_width"
                                       id="ps_crop_image_width" placeholder="eg. 300"
                                       value="<?php echo (!empty($ps_crop_image_width)) ? $ps_crop_image_width : 300; ?>">
                                <p class="cmb2-metabox-description"><?php _e('Image width value in pixel.', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>

                        <!--Image Height-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_crop_image_height"><?php _e('Image Height', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_crop_image_height"
                                       id="ps_crop_image_height" placeholder="eg. 200"
                                       value="<?php echo (!empty($ps_crop_image_height)) ? $ps_crop_image_height : 250; ?>">
                                <p class="cmb2-metabox-description"><?php _e('Image height value in pixel.', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <!--Display Post Title ? -->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label
                                    for="ps_display_post_title"><?php _e('Display Post Title', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_display_post_title"
                                                   id="ps_display_post_title"
                                                   value="yes" <?php if ($ps_display_post_title != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Enable it to show the Post Title.', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>

                        <!--Large Block Title Excerpt Length-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_title_excerpt_length"><?php _e('Large Block Title Limit', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_title_excerpt_length"
                                       id="ps_title_excerpt_length" placeholder="eg. 5"
                                       value="<?php echo (!empty($ps_title_excerpt_length)) ? $ps_title_excerpt_length : 5; ?>">
                                <p class="cmb2-metabox-description"><?php _e('Insert the number of words you would like to show at title', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>

                        <!--Small Block Title Excerpt Length-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_small_title_excerpt_length"><?php _e('Small Block Title Limit', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_small_title_excerpt_length"
                                       id="ps_small_title_excerpt_length" placeholder="eg. 5"
                                       value="<?php echo (!empty($ps_small_title_excerpt_length)) ? $ps_small_title_excerpt_length : 4; ?>">
                                <p class="cmb2-metabox-description"><?php _e('Insert the number of words you would like to show at title', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <!--Display Date ? -->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label
                                    for="ps_display_post_date"><?php _e('Display Post Date', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_display_post_date"
                                                   id="ps_display_post_date"
                                                   value="yes" <?php if ($ps_display_post_date != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Enable it to show the Post Date under the title.', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>


                        <!--Show Excerpt ? -->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label for="ps_display_excerpt"><?php _e('Display Content', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_display_excerpt" id="ps_display_excerpt"
                                                   value="yes" <?php if ($ps_display_excerpt != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Enable it to show the Post Excerpt.', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>


                        <!--Large Block Excerpt Length-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_excerpt_length"><?php _e('Large Block Content Length', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_excerpt_length"
                                       id="ps_excerpt_length" placeholder="eg. 50"
                                       value="<?php echo (!empty($ps_excerpt_length)) ? $ps_excerpt_length : 50; ?>">
                                <p class="cmb2-metabox-description"><?php _e('Insert the number of words you would like to show as Excerpt.', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>

                        <!--Small Block Excerpt Length-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_small_excerpt_length"><?php _e('Small Block Content Length', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_small_excerpt_length"
                                       id="ps_small_excerpt_length" placeholder="eg. 35"
                                       value="<?php echo (!empty($ps_small_excerpt_length)) ? $ps_small_excerpt_length : 35; ?>">
                                <p class="cmb2-metabox-description"><?php _e('Insert the number of words you would like to show as Excerpt.', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div id="tab-2" class="tab-content">
                <div class="cmb2-wrap form-table">
                    <div id="cmb2-metabox" class="cmb2-metabox cmb-field-list">

                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label for="ps_auto_play"><?php _e('Auto Play', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_auto_play" id="ps_auto_play"
                                                   value="yes" <?php if ($ps_auto_play != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Play slider\'s slide automatically ? ', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <!--Stop on Hover-->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label for="ps_stop_on_hover"><?php _e('Stop on Hover', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_stop_on_hover" id="ps_stop_on_hover"
                                                   value="yes" <?php if ($ps_stop_on_hover != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Stop slider\'s slide autoplay on mouse hover ?', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <!--Display Navigation Arrows-->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label
                                    for="ps_display_header"><?php _e('Display Navigation Arrows', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_display_navigation_arrows"
                                                   id="ps_display_navigation_arrows"
                                                   value="yes" <?php if ($ps_display_navigation_arrows != 'no') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Display slider Navigation Arrow or not', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>

                        <!--Show Dots Pagination-->
                        <div class="cmb-row cmb-type-radio">
                            <div class="cmb-th">
                                <label for="ps_pagination"><?php _e('Show Dots Pagination', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <ul class="cmb2-radio-list cmb2-list">
                                    <li><label class="checkbox">
                                            <input type="checkbox" name="ps_pagination" id="ps_pagination"
                                                   value="yes" <?php if ($ps_pagination == 'yes') {
                                                echo 'checked';
                                            } ?> class="checkbox__input"/>
                                            <div class="checkbox__switch"></div>
                                        </label>
                                    </li>
                                </ul>
                                <p class="cmb2-metabox-description"><?php _e('Show dots pagination below the slider?', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label for="ps_slide_speed"><?php _e('Slide Speed', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_slide_speed" id="ps_slide_speed"
                                       placeholder="1000 =1 Sec" value="<?php if (!empty($ps_slide_speed)) {
                                    echo $ps_slide_speed;
                                } else {
                                    echo 5000;
                                } ?>">
                                <p class="cmb2-metabox-description"><?php _e('1000 means 1 second.', PS_TEXTDOMAIN); ?></p>

                            </div>
                        </div>

                        <!--Posts on Desktop-->
                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label
                                    for="ps_item_on_desktop"><?php _e('Show Posts on Desktop', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_item_on_desktop"
                                       id="ps_item_on_desktop" value="<?php if (!empty($ps_item_on_desktop)) {
                                    echo $ps_item_on_desktop;
                                } else {
                                    echo 4;
                                } ?>">
                                <p class="cmb2-metabox-description"><?php _e('Maximum amount of posts to display at a time on Desktop or Large Screen Devices.', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <!--Posts on Tablet-->

                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label
                                    for="ps_item_on_tablet"><?php _e('Show Posts on Tablet', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_item_on_tablet"
                                       id="ps_item_on_tablet" value="<?php if (!empty($ps_item_on_tablet)) {
                                    echo $ps_item_on_tablet;
                                } else {
                                    echo 2;
                                } ?>">
                                <p class="cmb2-metabox-description"><?php _e('Maximum amount of posts to display at a time on Tablet Screen.', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>

                        <!--Posts on Mobile-->

                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label
                                    for="ps_item_on_mobile"><?php _e('Show Posts on Mobile', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_item_on_mobile"
                                       id="ps_item_on_mobile" value="<?php if (!empty($ps_item_on_mobile)) {
                                    echo $ps_item_on_mobile;
                                } else {
                                    echo 2;
                                } ?>">
                                <p class="cmb2-metabox-description"><?php _e('Maximum amount of posts to display at a time on Mobile Screen.', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <div id="tab-3" class="tab-content">

                <div class="cmb2-wrap form-table">
                    <div id="cmb2-metabox" class="cmb2-metabox cmb-field-list">

                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label
                                    for="ps_header_title_font_size"><?php _e('Slider Title Font Size', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_header_title_font_size"
                                       id="ps_header_title_font_size"
                                       value="<?php if (!empty($ps_header_title_font_size)) {
                                           echo $ps_header_title_font_size;
                                       } else {
                                           echo "20px";
                                       } ?>" placeholder="e.g. 20px">
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_header_title_font_color"><?php _e('Slider Title Font Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_header_title_font_color"
                                       id="ps_header_title_font_color"
                                       value="<?php if (!empty($ps_header_title_font_color)) {
                                           echo $ps_header_title_font_color;
                                       } else {
                                           echo "#303030";
                                       } ?>">
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_nav_arrow_color"><?php _e('Navigational Arrow Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_nav_arrow_color"
                                       id="ps_nav_arrow_color" value="<?php if (!empty($ps_nav_arrow_color)) {
                                    echo $ps_nav_arrow_color;
                                } else {
                                    echo "#FFFFFF";
                                } ?>">
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_nav_arrow_bg_color"><?php _e('Navigational Arrow Background Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_nav_arrow_bg_color"
                                       id="ps_nav_arrow_bg_color"
                                       value="<?php echo (!empty($ps_nav_arrow_bg_color)) ? $ps_nav_arrow_bg_color : "#686868"; ?>">
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_nav_arrow_hover_color"><?php _e('Navigational Arrow Hover Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_nav_arrow_hover_color"
                                       id="ps_nav_arrow_hover_color"
                                       value="<?php if (!empty($ps_nav_arrow_hover_color)) {
                                           echo $ps_nav_arrow_hover_color;
                                       } else {
                                           echo "#FFFFFF";
                                       } ?>">
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_nav_arrow_bg_hover_color"><?php _e('Navigational Arrow Background Hover Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_nav_arrow_bg_hover_color"
                                       id="ps_nav_arrow_bg_hover_color"
                                       value="<?php if (!empty($ps_nav_arrow_bg_hover_color)) {
                                           echo $ps_nav_arrow_bg_hover_color;
                                       } else {
                                           echo "#474747";
                                       } ?>">
                            </div>
                        </div>

                        <!--Border color for theme B and D-->
                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_border_color"><?php _e('Slider Border Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_border_color" id="ps_border_color"
                                       value="<?php if (!empty($ps_border_color)) {
                                           echo $ps_border_color;
                                       } else {
                                           echo "#f7f7f7";
                                       } ?>">
                                <p class="cmb2-metabox-description"><?php _e('Border Color if you use "THEME B" or "THEME D" ', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>

                        <!--Border Hover color for theme B and D-->
                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_border_hover_color"><?php _e('Slider Border Hover Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_border_hover_color"
                                       id="ps_border_hover_color" value="<?php if (!empty($ps_border_hover_color)) {
                                    echo $ps_border_hover_color;
                                } else {
                                    echo "#ececec";
                                } ?>">
                                <p class="cmb2-metabox-description"><?php _e('Border Hover Color if you use "THEME B" or "THEME D" ', PS_TEXTDOMAIN); ?></p>
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-text-medium">
                            <div class="cmb-th">
                                <label
                                    for="ps_title_font_size"><?php _e('Post Title Font Size', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_title_font_size"
                                       placeholder="eg. 16px"
                                       id="ps_title_font_size"
                                       value="<?php if (!empty($ps_title_font_size)) {
                                           echo $ps_title_font_size;
                                       } else {
                                           echo "16px";
                                       } ?>">
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_title_font_color"><?php _e('Post Title Font Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_title_font_color"
                                       id="ps_title_font_color" value="<?php if (!empty($ps_title_font_color)) {
                                    echo $ps_title_font_color;
                                } else {
                                    echo "#303030";
                                } ?>">
                            </div>
                        </div>


                        <div class="cmb-row cmb-type-colorpicker">
                            <div class="cmb-th">
                                <label
                                    for="ps_title_hover_font_color"><?php _e('Post Title Hover Font Color', PS_TEXTDOMAIN); ?></label>
                            </div>
                            <div class="cmb-td">
                                <input type="text" class="cmb2-text-small" name="ps_title_hover_font_color"
                                       id="ps_title_hover_font_color"
                                       value="<?php if (!empty($ps_title_hover_font_color)) {
                                           echo $ps_title_hover_font_color;
                                       } else {
                                           echo "#000";
                                       } ?>">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div> <!-- end tab -->
    </div> <!-- end tabs-container -->
<?php }
