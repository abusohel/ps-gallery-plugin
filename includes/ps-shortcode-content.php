<!--  loop Starts -->
<?php

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

$i = 0;
while ($loop->have_posts()) : $loop->the_post();
    $i++;
    ?>
    <div class="ps-gallery-block <?php echo $col[$i] ?> <?php echo $ps_select_theme ?>">
        <?php (!empty($ps_select_theme)) ?
            include PS_PLUGIN_DIR . '/themes/' . $ps_select_theme . '.php'
            :
            include PS_PLUGIN_DIR . '/themes/gallerydefault.php';
        ?>
    </div>
<?php endwhile; ?>
<!--Loop Ends-->








