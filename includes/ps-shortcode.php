<?php

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

function pxlr_excerpt_more($more)
{
    return sprintf('<a class="read-more" href="%1$s">%2$s</a>',
        get_permalink(get_the_ID()),
        __(' ...read more → ', PS_TEXTDOMAIN)
    );
}

add_filter('excerpt_more', 'pxlr_excerpt_more');


/**
 * Prints the limited excerpts of the post
 * @param int $limit | Limit excerpt by a number of word. Default is 50.
 *
 * @return array|mixed|string
 */
function pxlr_get_limited_excerpts($limit = 50)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $moreText = apply_filters('excerpt_more', '...');
        $excerpt = implode(" ", $excerpt) . $moreText;
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return '<p>' . $excerpt . '</p>';
}


/**
 * Prints the shortcode for the posts slider
 * @param $atts
 * @param null $content
 *
 * @return null|string
 */
function pxlr_shortcode_output($atts, $content = null)
{
    ob_start();
    // get shortcode atts
    $atts = shortcode_atts(
        array(
            'id' => "",
        ), $atts);
    // enqueue styles and scripts for this shortcode
    wp_enqueue_style('pxlr-gallery-block-frontend');
    wp_enqueue_style('fontello-style');
    wp_enqueue_style('owl-carousel-min-style');
    wp_enqueue_style('owl-theme-default-min-style');
    wp_enqueue_script('owl-carousel-min-script');
    wp_enqueue_script('pxlr-front-end-script');

    $shorcode_id = $atts['id'];
    $random_pxlr_wrapper_id = rand();
    $random_button_id = rand();
    $nextButton = "pxlr-next-{$random_button_id}";
    $prevButton = "pxlr-prev-{$random_button_id}";


//  temp vars
    $pxlr_display_header = get_post_meta($shorcode_id, 'pxlr_display_header', true);
    $pxlr_select_theme = get_post_meta($shorcode_id, 'pxlr_select_theme', true);
    $pxlr_display_navigation_arrows = get_post_meta($shorcode_id, 'pxlr_display_navigation_arrows', true);
    $pxlr_title = get_post_meta($shorcode_id, 'pxlr_title', true);
    $pxlr_total_posts = get_post_meta($shorcode_id, 'pxlr_total_posts', true);

    //query type
    $pxlr_posts_type = get_post_meta($shorcode_id, 'pxlr_posts_type', true);
    $pxlr_posts_bycategory = get_post_meta($shorcode_id, 'pxlr_posts_bycategory', true);
    $pxlr_posts_byID = get_post_meta($shorcode_id, 'pxlr_posts_byID', true);
    $pxlr_posts_byTag = get_post_meta($shorcode_id, 'pxlr_posts_byTag', true);
    $pxlr_posts_by_year = get_post_meta($shorcode_id, 'pxlr_posts_by_year', true);
    $pxlr_posts_from_month = get_post_meta($shorcode_id, 'pxlr_posts_from_month', true);
    $pxlr_posts_from_month_year = get_post_meta($shorcode_id, 'pxlr_posts_from_month_year', true);


    $pxlr_display_placeholder_img = get_post_meta($shorcode_id, 'pxlr_display_placeholder_img', true);
    $pxlr_default_feat_img = get_post_meta($shorcode_id, 'pxlr_default_feat_img', true);
    $pxlr_display_img = get_post_meta($shorcode_id, 'pxlr_display_img', true);


    $pxlr_image_crop = get_post_meta($shorcode_id, 'pxlr_image_crop', true);
    $pxlr_crop_image_width = get_post_meta($shorcode_id, 'pxlr_crop_image_width', true);
    $pxlr_crop_image_height = get_post_meta($shorcode_id, 'pxlr_crop_image_height', true);
    $pxlr_display_post_title = get_post_meta($shorcode_id, 'pxlr_display_post_title', true);
    $pxlr_display_post_date = get_post_meta($shorcode_id, 'pxlr_display_post_date', true);
    $pxlr_display_excerpt = get_post_meta($shorcode_id, 'pxlr_display_excerpt', true);
    $pxlr_title_excerpt_length = get_post_meta($shorcode_id, 'pxlr_title_excerpt_length', true);
    $pxlr_small_title_excerpt_length = get_post_meta($shorcode_id, 'pxlr_small_title_excerpt_length', true);
    $pxlr_excerpt_length = get_post_meta($shorcode_id, 'pxlr_excerpt_length', true);
    $pxlr_small_excerpt_length = get_post_meta($shorcode_id, 'pxlr_small_excerpt_length', true);

    $pxlr_auto_play = get_post_meta($shorcode_id, 'pxlr_auto_play', true);
    $pxlr_stop_on_hover = get_post_meta($shorcode_id, 'pxlr_stop_on_hover', true);
    $pxlr_slide_speed = get_post_meta($shorcode_id, 'pxlr_slide_speed', true);
    $pxlr_item_on_desktop = get_post_meta($shorcode_id, 'pxlr_item_on_desktop', true);
    $pxlr_item_on_tablet = get_post_meta($shorcode_id, 'pxlr_item_on_tablet', true);
    $pxlr_item_on_mobile = get_post_meta($shorcode_id, 'pxlr_item_on_mobile', true);
    $pxlr_pagination = get_post_meta($shorcode_id, 'pxlr_pagination', true);

    $pxlr_header_title_font_size = get_post_meta($shorcode_id, 'pxlr_header_title_font_size', true);
    $pxlr_header_title_font_color = get_post_meta($shorcode_id, 'pxlr_header_title_font_color', true);
    $pxlr_nav_arrow_color = get_post_meta($shorcode_id, 'pxlr_nav_arrow_color', true);
    $pxlr_nav_arrow_bg_color = get_post_meta($shorcode_id, 'pxlr_nav_arrow_bg_color', true);
    $pxlr_nav_arrow_hover_color = get_post_meta($shorcode_id, 'pxlr_nav_arrow_hover_color', true);
    $pxlr_nav_arrow_bg_hover_color = get_post_meta($shorcode_id, 'pxlr_nav_arrow_bg_hover_color', true);

    $pxlr_border_color = get_post_meta($shorcode_id, 'pxlr_border_color', true);
    $pxlr_border_hover_color = get_post_meta($shorcode_id, 'pxlr_border_hover_color', true);

    $pxlr_title_font_size = get_post_meta($shorcode_id, 'pxlr_title_font_size', true);
    $pxlr_title_font_color = get_post_meta($shorcode_id, 'pxlr_title_font_color', true);
    $pxlr_title_hover_font_color = get_post_meta($shorcode_id, 'pxlr_title_hover_font_color', true);


    // sanitaized vars
    $pxlr_display_header = (!empty($pxlr_display_header)) ? esc_attr($pxlr_display_header) : '';
    $pxlr_select_theme = (!empty($pxlr_select_theme)) ? esc_attr($pxlr_select_theme) : '';
    $pxlr_display_navigation_arrows = (!empty($pxlr_display_navigation_arrows)) ? esc_attr($pxlr_display_navigation_arrows) : '';
    $pxlr_title = (!empty($pxlr_title)) ? esc_attr($pxlr_title) : '';
    $pxlr_total_posts = (!empty($pxlr_total_posts)) ? esc_attr($pxlr_total_posts) : 12;
    $pxlr_posts_type = (!empty($pxlr_posts_type)) ? esc_attr($pxlr_posts_type) : '';
    $pxlr_posts_bycategory = (!empty($pxlr_posts_bycategory)) ? esc_attr($pxlr_posts_bycategory) : '';
    $pxlr_posts_byID = (!empty($pxlr_posts_byID)) ? esc_attr($pxlr_posts_byID) : '';
    $pxlr_posts_byTag = (!empty($pxlr_posts_byTag)) ? esc_attr($pxlr_posts_byTag) : '';
    $pxlr_posts_by_year = (!empty($pxlr_posts_by_year)) ? esc_attr($pxlr_posts_by_year) : '';
    $pxlr_posts_from_month = (!empty($pxlr_posts_from_month)) ? esc_attr($pxlr_posts_from_month) : '';
    $pxlr_posts_from_month_year = (!empty($pxlr_posts_from_month_year)) ? esc_attr($pxlr_posts_from_month_year) : '';
    $pxlr_display_placeholder_img = (!empty($pxlr_display_placeholder_img)) ? esc_attr($pxlr_display_placeholder_img) : '';
    $pxlr_default_feat_img = (!empty($pxlr_default_feat_img)) ? esc_attr($pxlr_default_feat_img) : '';
    $pxlr_display_img = (!empty($pxlr_display_img)) ? esc_attr($pxlr_display_img) : '';
    $pxlr_image_crop = (!empty($pxlr_image_crop)) ? esc_attr($pxlr_image_crop) : '';
    $pxlr_crop_image_width = (!empty($pxlr_crop_image_width)) ? absint(esc_attr($pxlr_crop_image_width)) : '';
    $pxlr_crop_image_height = (!empty($pxlr_crop_image_height)) ? absint(esc_attr($pxlr_crop_image_height)) : '';
    $pxlr_display_post_title = (!empty($pxlr_display_post_title)) ? esc_attr($pxlr_display_post_title) : '';
    $pxlr_display_post_date = (!empty($pxlr_display_post_date)) ? esc_attr($pxlr_display_post_date) : '';
    $pxlr_display_excerpt = (!empty($pxlr_display_excerpt)) ? esc_attr($pxlr_display_excerpt) : '';
    $pxlr_excerpt_length = (!empty($pxlr_excerpt_length)) ? absint(esc_attr($pxlr_excerpt_length)) : '';
    $pxlr_small_excerpt_length = (!empty($pxlr_small_excerpt_length)) ? absint(esc_attr($pxlr_small_excerpt_length)) : '';
    $pxlr_title_excerpt_length = (!empty($pxlr_title_excerpt_length)) ? absint(esc_attr($pxlr_title_excerpt_length)) : '';
    $pxlr_small_title_excerpt_length = (!empty($pxlr_small_title_excerpt_length)) ? absint(esc_attr($pxlr_small_title_excerpt_length)) : '';
    $pxlr_auto_play = (!empty($pxlr_auto_play)) ? esc_attr($pxlr_auto_play) : '';
    $pxlr_stop_on_hover = (!empty($pxlr_stop_on_hover)) ? esc_attr($pxlr_stop_on_hover) : '';
    $pxlr_slide_speed = (!empty($pxlr_slide_speed)) ? esc_attr($pxlr_slide_speed) : 5000;
    $pxlr_item_on_desktop = (!empty($pxlr_item_on_desktop)) ? absint(esc_attr($pxlr_item_on_desktop)) : 4;
    $pxlr_item_on_tablet = (!empty($pxlr_item_on_tablet)) ? absint(esc_attr($pxlr_item_on_tablet)) : 3;
    $pxlr_item_on_mobile = (!empty($pxlr_item_on_mobile)) ? absint(esc_attr($pxlr_item_on_mobile)) : 2;
    $pxlr_pagination = (!empty($pxlr_pagination)) ? esc_attr($pxlr_pagination) : '';
    $pxlr_header_title_font_size = (!empty($pxlr_header_title_font_size)) ? esc_attr($pxlr_header_title_font_size) : '';
    $pxlr_header_title_font_color = (!empty($pxlr_header_title_font_color)) ? esc_attr($pxlr_header_title_font_color) : '';
    $pxlr_nav_arrow_color = (!empty($pxlr_nav_arrow_color)) ? esc_attr($pxlr_nav_arrow_color) : '';
    $pxlr_nav_arrow_bg_color = (!empty($pxlr_nav_arrow_bg_color)) ? esc_attr($pxlr_nav_arrow_bg_color) : '';
    $pxlr_nav_arrow_hover_color = (!empty($pxlr_nav_arrow_hover_color)) ? esc_attr($pxlr_nav_arrow_hover_color) : '';
    $pxlr_nav_arrow_bg_hover_color = (!empty($pxlr_nav_arrow_bg_hover_color)) ? esc_attr($pxlr_nav_arrow_bg_hover_color) : '';
    $pxlr_border_color = (!empty($pxlr_border_color)) ? esc_attr($pxlr_border_color) : '';
    $pxlr_border_hover_color = (!empty($pxlr_border_hover_color)) ? esc_attr($pxlr_border_hover_color) : '';
    $pxlr_title_font_size = (!empty($pxlr_title_font_size)) ? esc_attr($pxlr_title_font_size) : '';
    $pxlr_title_font_color = (!empty($pxlr_title_font_color)) ? esc_attr($pxlr_title_font_color) : '';
    $pxlr_title_hover_font_color = (!empty($pxlr_title_hover_font_color)) ? esc_attr($pxlr_title_hover_font_color) : '';


//  Build the args for query.
    $args = [];
    $common_args = [
        'post_type' => 'post',
        'posts_per_page' => (!empty($pxlr_total_posts) ? absint($pxlr_total_posts) : 10),
        'status' => 'published',
        'no_found_rows' => true, // remove if pagination needed

    ];
    // for testing, setting this equal
    if ('latest' == $pxlr_posts_type) {
        $args = $common_args;
    } elseif ('older' == $pxlr_posts_type) {
        $older_args = [
            'orderby' => 'date',
            'order' => 'ASC',
        ];
        $args = array_merge($common_args, $older_args);
    } elseif ('featured' == $pxlr_posts_type) {
        $featured_posts_args = [
            'meta_query' => [
                [
                    'key' => '_is_featured',
                    'value' => 'yes',
                ]
            ]];
        $args = array_merge($common_args, $featured_posts_args);
    } elseif ('popular_post' == $pxlr_posts_type) {
        $popular_posts_args = [
            'meta_key' => '_pxlr_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'ignore_sticky_posts' => true,
        ];
        $args = array_merge($common_args, $popular_posts_args);
    } elseif ('category' == $pxlr_posts_type) {
        $posts_bycategory_args = [
            'category_name' => $pxlr_posts_bycategory,
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        ];

        $args = array_merge($common_args, $posts_bycategory_args);

    } elseif ('postsbyid' == $pxlr_posts_type) {
        $all_posts = explode(',', $pxlr_posts_byID);
        $posts_byID_args = [
            'post__in' => $all_posts,
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        ];

        $args = array_merge($common_args, $posts_byID_args);

    } elseif ('postsbytag' == $pxlr_posts_type) {
        $all_posts_tag = explode(',', $pxlr_posts_byTag);
        $posts_byTag_args = [
            'tag_slug__in' => $all_posts_tag,
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        ];
        $args = array_merge($common_args, $posts_byTag_args);
    } else {
        $args = $common_args;
    }


    // LOOP FOR POSTS CUSTOM POST INITIATED
    $loop = new WP_Query($args);

    // fix repeat post problem if post count is less than post to display by slider
    $post_found_count = $loop->post_count;
    $pxlr_item_on_desktop = ($post_found_count >= $pxlr_item_on_desktop) ? $pxlr_item_on_desktop : $post_found_count;
    $pxlr_item_on_tablet = ($post_found_count >= $pxlr_item_on_tablet) ? $pxlr_item_on_tablet : $post_found_count;
    $pxlr_item_on_mobile = ($post_found_count >= $pxlr_item_on_mobile) ? $pxlr_item_on_mobile : $post_found_count;

    $order = '';
    $post = '';
    $col[] = '';

    for($i=1;$i <= $post_found_count;$i++){

        if ('gallerydefault' == $pxlr_select_theme) {
            $col[$i] = 'pxlr-gallery-block-small col-md-4';

        } elseif ('gallery23' == $pxlr_select_theme) {
            if (in_array($i, array(1, 2)))
                $col[$i] = 'pxlr-gallery-block-large col-md-6';
            else
                $col[$i] = 'pxlr-gallery-block-small col-md-4';

        } elseif ('gallery212' == $pxlr_select_theme) {

            if (2 == $i) {
                $col[$i] = 'pxlr-gallery-block-large col-md-6';
            } else {
                $col[$i] = 'pxlr-gallery-block-small col-md-3';
            }

        } elseif ('gallery1221' == $pxlr_select_theme) {

            if (in_array($i, array(1, 6))) {
                $col[$i] = 'pxlr-gallery-block-large col-md-6';
            } else {
                $col[$i] = 'pxlr-gallery-block-small col-md-3';
            }

        } elseif ('gallery122' == $pxlr_select_theme) {
            if (1==$i) {
                $col[$i] = 'pxlr-gallery-block-large col-md-6';
            } else {
                $col[$i] = 'pxlr-gallery-block-small col-md-3';
            }
        } elseif ('gallerycarousel' == $pxlr_select_theme) {
            $col[$i] = 'pxlr-gallery-block-small col-sm-6 col-md-3';
        } else {

        }
    }

    if ($loop->have_posts() && $pxlr_select_theme == 'gallerycarousel'): ?>

        <!--STYLES FOR posts -->
        <?php include( 'ps-style.php' ); ?>


        <div class='header-<?php echo $random_pxlr_wrapper_id; ?> pxlr-gallery-main-header'>
            <!--have to apply a header style option for title because it is from shortcode generator-->
            <?php if ('yes' == $pxlr_display_header) echo "<h1 class='pxlr-title'>{$pxlr_title}</h1>";


            ?>
        </div> <!-- ends header -->

        <!--outer_wrap to position nav arrows-->
        <div class="outer_wrap-<?php echo $random_pxlr_wrapper_id; ?>">
        <!--show navigation arrows-->
        <?php
        if ('no' != $pxlr_display_navigation_arrows) {
            echo '<button class="pxlr-gallery-carousel ' . $prevButton . '">  <i class="zmdi zmdi-chevron-left"></i> </button>
                      <button class="pxlr-gallery-carousel ' . $nextButton . '"> <i class="zmdi zmdi-chevron-right"></i> </button>';
        }
        ?>

        <!--Main wrapper for the slider STARTS-->
        <div id="pxlr-slider-wrapper-<?php echo $random_pxlr_wrapper_id; ?>"
             class="owl-theme owl-carousel pxlr-slider-wrapper-class pxlr-<?php echo $random_pxlr_wrapper_id; ?> <?php echo $pxlr_select_theme ?>">

            <?php

            //include shortcode content
            include( 'ps-shortcode-content-carousel.php' );

            wp_reset_postdata();
            ?>
            <script>
                jQuery(document).ready(function ($) {
                    var postSlider = $(".pxlr-<?php echo $random_pxlr_wrapper_id; ?>");
                    postSlider.owlCarousel({
                        margin: 20,
                        loop: true,
                        autoWidth: false,
                        responsiveClass: true,
                        dots: false,
                        autoplay: true,

                        autoplayTimeout: <?php echo $pxlr_slide_speed; ?>,
                        autoplayHoverPause: false,
                        //dotData:true,
                        //dotsEach:true,
                        slideBy: 1,

                        responsive: {
                            0: {
                                items: 1,
                            },
                            500: {
                                items:<?php echo $pxlr_item_on_mobile;?>,
                            },
                            600: {

                                items:<?php echo ($pxlr_item_on_tablet > 1) ? $pxlr_item_on_tablet - 1 : $pxlr_item_on_tablet;?>,

                            },
                            768: {
                                items:<?php echo $pxlr_item_on_tablet;?>,

                            },
                            1199: {
                                items:<?php echo $pxlr_item_on_desktop;?>,

                            }
                        }
                    });

                    // custom navigation button for slider
                    // Go to the next item
                    $('.pxlr-next-<?php echo $random_button_id;?>').click(function () {
                        postSlider.trigger('next.owl.carousel');
                    });
                    // Go to the previous item
                    $('.pxlr-prev-<?php echo $random_button_id;?>').click(function () {
                        // With optional speed parameter
                        // Parameters has to be in square bracket '[]'
                        postSlider.trigger('prev.owl.carousel');
                    });

                    // stop on hover but play after hover out
                    <?php if(!empty($pxlr_stop_on_hover)){ ?>
                    postSlider.hover(
                        function () {
                            postSlider.trigger('stop.owl.autoplay');
                        },
                        function () {
                            postSlider.trigger('play.owl.autoplay');
                        }
                    );
                    <?php } ?>


                });
            </script>
        </div> <!--    ends div.outer_wrap -->

    <?php elseif ($pxlr_select_theme !== 'gallerycarousel') : ?>


        <div class="gallery-container">
            <div class="gallery-container-wrap">
                <div class="row">
                    <div class="col-md-12">
                        <div class='header-<?php echo $random_pxlr_wrapper_id; ?> pxlr-gallery-main-header'>
                            <!--have to apply a header style option for title because it is from shortcode generator-->
                            <div class='header-<?php echo $random_pxlr_wrapper_id; ?> pxlr-gallery-main-header'>
                                <!--have to apply a header style option for title because it is from shortcode generator-->
                                <?php if ('yes' == $pxlr_display_header) echo "<h1 class='pxlr-title'>{$pxlr_title}</h1>";?>
                            </div> <!-- ends header -->
                        </div> <!-- ends header -->
                    </div>
                    <?php
                    //include shortcode content
                    include( 'ps-shortcode-content.php' );
                    ?>
                </div>
            </div>
        </div>


    <?php else:
        _e('No Post Found.', PS_TEXTDOMAIN);
    endif; // if($loop->have_posts() ends
    ?>


    <?php
    $content = ob_get_clean();
    return $content;
}

add_shortcode('pxlr-gallery-block', 'pxlr_shortcode_output');