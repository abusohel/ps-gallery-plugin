<?php

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

function ps_meta_save( $post_id, $post ) {
    // the following line is needed because we will hook into edit_post hook, so that we can set default value of checkbox.
    if ($post->post_type != 'psgalleryblock') {return;}
    // Perform checking for before saving
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['ps_meta_save_nounce']) && wp_verify_nonce( $_POST['ps_meta_save_nounce'], 'ps_meta_save' )? 'true': 'false');

    if ( $is_autosave || $is_revision || !$is_valid_nonce ) return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post_id )) return;

    // get all data to save
    // General Settings
    $ps_display_header = (isset($_POST['ps_display_header']))? sanitize_text_field( $_POST["ps_display_header"] ): 'no';
    $ps_display_navigation_arrows = (isset($_POST['ps_display_navigation_arrows']))? sanitize_text_field( $_POST["ps_display_navigation_arrows"] ): 'no';
    $ps_title = (isset($_POST['ps_title']))? sanitize_text_field( $_POST["ps_title"] ): '';
    $ps_total_posts = (isset($_POST['ps_total_posts']))? sanitize_text_field( $_POST["ps_total_posts"] ): 0 ;
    $ps_select_theme = (isset($_POST['ps_select_theme']))? sanitize_text_field( $_POST["ps_select_theme"] ): 0 ;
    $ps_posts_type = (isset($_POST['ps_posts_type']))? sanitize_text_field( $_POST["ps_posts_type"] ): 0 ;
    $ps_posts_bycategory = (isset($_POST['ps_posts_bycategory']))? sanitize_text_field( $_POST["ps_posts_bycategory"] ): '' ;
    $ps_posts_byID = (isset($_POST['ps_posts_byID']))? sanitize_text_field( $_POST["ps_posts_byID"] ): 0 ;
    $ps_posts_byTag = (isset($_POST['ps_posts_byTag']))? sanitize_text_field( $_POST["ps_posts_byTag"] ): '' ;
    $ps_posts_by_year = (isset($_POST['ps_posts_by_year']))? sanitize_text_field( $_POST["ps_posts_by_year"] ): 0 ;
    $ps_posts_from_month = (isset($_POST['ps_posts_from_month']))? sanitize_text_field( $_POST["ps_posts_from_month"] ): 0 ;
    $ps_posts_from_month_year = (isset($_POST['ps_posts_from_month_year']))? sanitize_text_field( $_POST["ps_posts_from_month_year"] ): 0 ;

    $ps_display_placeholder_img = (isset($_POST['ps_display_placeholder_img']))? sanitize_text_field( $_POST["ps_display_placeholder_img"] ): 'no' ;
    $ps_default_feat_img = (isset($_POST['ps_default_feat_img']))? sanitize_text_field( $_POST["ps_default_feat_img"] ): '' ;
    $ps_display_img = (isset($_POST['ps_display_img']))? sanitize_text_field( $_POST["ps_display_img"] ): 'no' ;
    $ps_image_crop = (isset($_POST['ps_image_crop']))? sanitize_text_field( $_POST["ps_image_crop"] ): 'no' ;
    $ps_crop_image_width = (isset($_POST['ps_crop_image_width']))? sanitize_text_field( $_POST["ps_crop_image_width"] ): 0 ;
    $ps_crop_image_height = (isset($_POST['ps_crop_image_height']))? sanitize_text_field( $_POST["ps_crop_image_height"] ): 0 ;
    $ps_display_post_title = (isset($_POST['ps_display_post_title']))? sanitize_text_field( $_POST["ps_display_post_title"] ): 'no' ;
    $ps_display_post_date = (isset($_POST['ps_display_post_date']))? sanitize_text_field( $_POST["ps_display_post_date"] ): 'no' ;
    $ps_display_excerpt = (isset($_POST['ps_display_excerpt']))? sanitize_text_field( $_POST["ps_display_excerpt"] ): 'no' ;
    $ps_excerpt_length = (isset($_POST['ps_excerpt_length']))? sanitize_text_field( $_POST["ps_excerpt_length"] ): 0 ;
    $ps_small_excerpt_length = (isset($_POST['ps_small_excerpt_length']))? sanitize_text_field( $_POST["ps_small_excerpt_length"] ): 0 ;
    $ps_title_excerpt_length = (isset($_POST['ps_title_excerpt_length']))? sanitize_text_field( $_POST["ps_title_excerpt_length"] ): 0 ;
    $ps_small_title_excerpt_length = (isset($_POST['ps_small_title_excerpt_length']))? sanitize_text_field( $_POST["ps_small_title_excerpt_length"] ): 0 ;

    // Slider Settings

    $ps_auto_play = (isset($_POST['ps_auto_play']))? sanitize_text_field( $_POST["ps_auto_play"] ): 'no' ;
    $ps_stop_on_hover = (isset($_POST['ps_stop_on_hover']))? sanitize_text_field( $_POST["ps_stop_on_hover"] ): 'no' ;
    $ps_slide_speed = (isset($_POST['ps_slide_speed']))? sanitize_text_field( $_POST["ps_slide_speed"] ): 0 ;
    $ps_item_on_desktop = (isset($_POST['ps_item_on_desktop']))? sanitize_text_field( $_POST["ps_item_on_desktop"] ): 0 ;
    $ps_item_on_tablet = (isset($_POST['ps_item_on_tablet']))? sanitize_text_field( $_POST["ps_item_on_tablet"] ): 0 ;
    $ps_item_on_mobile = (isset($_POST['ps_item_on_mobile']))? sanitize_text_field( $_POST["ps_item_on_mobile"] ): 0 ;
    $ps_pagination = (isset($_POST['ps_pagination']))? sanitize_text_field( $_POST["ps_pagination"] ): 'no' ;

    $ps_header_title_font_size = (isset($_POST['ps_header_title_font_size']))? sanitize_text_field( $_POST["ps_header_title_font_size"] ): '' ;
    $ps_header_title_font_color = (isset($_POST['ps_header_title_font_color']))? sanitize_text_field( $_POST["ps_header_title_font_color"] ): '' ;
    $ps_nav_arrow_color = (isset($_POST['ps_nav_arrow_color']))? sanitize_text_field( $_POST["ps_nav_arrow_color"] ): '' ;
    $ps_nav_arrow_bg_color = (isset($_POST['ps_nav_arrow_bg_color']))? sanitize_text_field( $_POST["ps_nav_arrow_bg_color"] ): '' ;
    $ps_nav_arrow_hover_color = (isset($_POST['ps_nav_arrow_hover_color']))? sanitize_text_field( $_POST["ps_nav_arrow_hover_color"] ): '' ;
    $ps_nav_arrow_bg_hover_color = (isset($_POST['ps_nav_arrow_bg_hover_color']))? sanitize_text_field( $_POST["ps_nav_arrow_bg_hover_color"] ): '' ;
    $ps_border_color = (isset($_POST['ps_border_color']))? sanitize_text_field( $_POST["ps_border_color"] ): '' ;
    $ps_border_hover_color = (isset($_POST['ps_border_hover_color']))? sanitize_text_field( $_POST["ps_border_hover_color"] ): '' ;
    $ps_title_font_size = (isset($_POST['ps_title_font_size']))? sanitize_text_field( $_POST["ps_title_font_size"] ): '' ;
    $ps_title_font_color = (isset($_POST['ps_title_font_color']))? sanitize_text_field( $_POST["ps_title_font_color"] ): '' ;
    $ps_title_hover_font_color = (isset($_POST['ps_title_hover_font_color']))? sanitize_text_field( $_POST["ps_title_hover_font_color"] ): '' ;






    // Save Meta data to the db
    //General Settings
    update_post_meta($post_id, "ps_display_header", $ps_display_header);
    //update_post_meta($post_id, "ps_display_navigation_arrows", $ps_display_navigation_arrows);
    update_post_meta($post_id, "ps_title", $ps_title);
    update_post_meta($post_id, "ps_total_posts", $ps_total_posts);

    update_post_meta($post_id, "ps_select_theme", $ps_select_theme);
    update_post_meta($post_id, "ps_posts_type", $ps_posts_type);
    update_post_meta($post_id, "ps_posts_bycategory", $ps_posts_bycategory);
    update_post_meta($post_id, "ps_posts_byID", $ps_posts_byID);
    update_post_meta($post_id, "ps_posts_byTag", $ps_posts_byTag);
    update_post_meta($post_id, "ps_posts_by_year", $ps_posts_by_year);
    update_post_meta($post_id, "ps_posts_from_month", $ps_posts_from_month);
    update_post_meta($post_id, "ps_posts_from_month_year", $ps_posts_from_month_year);


    update_post_meta($post_id, "ps_image_crop", $ps_image_crop);
    update_post_meta($post_id, "ps_crop_image_width", $ps_crop_image_width);
    update_post_meta($post_id, "ps_crop_image_height", $ps_crop_image_height);
    update_post_meta($post_id, "ps_display_post_title", $ps_display_post_title);
    update_post_meta($post_id, "ps_display_post_date", $ps_display_post_date);
    update_post_meta($post_id, "ps_display_excerpt", $ps_display_excerpt);
    update_post_meta($post_id, "ps_title_excerpt_length", $ps_title_excerpt_length);
    update_post_meta($post_id, "ps_small_title_excerpt_length", $ps_small_title_excerpt_length);
    update_post_meta($post_id, "ps_excerpt_length", $ps_excerpt_length);
    update_post_meta($post_id, "ps_small_excerpt_length", $ps_small_excerpt_length);

    // Slider Settings
    update_post_meta($post_id, "ps_auto_play", $ps_auto_play);
    update_post_meta($post_id, "ps_stop_on_hover", $ps_stop_on_hover);
    update_post_meta($post_id, "ps_slide_speed", $ps_slide_speed);
    update_post_meta($post_id, "ps_item_on_desktop", $ps_item_on_desktop);
    update_post_meta($post_id, "ps_item_on_tablet", $ps_item_on_tablet);
    update_post_meta($post_id, "ps_item_on_mobile", $ps_item_on_mobile);
    update_post_meta($post_id, "ps_pagination", $ps_pagination);

    update_post_meta($post_id, "ps_display_placeholder_img", $ps_display_placeholder_img);
    update_post_meta($post_id, "ps_default_feat_img", $ps_default_feat_img);
    update_post_meta($post_id, "ps_display_img", $ps_display_img);
    update_post_meta($post_id, "ps_header_title_font_size", $ps_header_title_font_size);
    update_post_meta($post_id, "ps_header_title_font_color", $ps_header_title_font_color);
    update_post_meta($post_id, "ps_nav_arrow_color", $ps_nav_arrow_color);
    update_post_meta($post_id, "ps_nav_arrow_bg_color", $ps_nav_arrow_bg_color);
    update_post_meta($post_id, "ps_nav_arrow_hover_color", $ps_nav_arrow_hover_color);
    update_post_meta($post_id, "ps_nav_arrow_bg_hover_color", $ps_nav_arrow_bg_hover_color);
    update_post_meta($post_id, "ps_border_color", $ps_border_color);
    update_post_meta($post_id, "ps_border_hover_color", $ps_border_hover_color);
    update_post_meta($post_id, "ps_title_font_size", $ps_title_font_size);
    update_post_meta($post_id, "ps_title_font_color", $ps_title_font_color);
    update_post_meta($post_id, "ps_title_hover_font_color", $ps_title_hover_font_color);
}

add_action( 'edit_post', 'ps_meta_save', 10, 2);