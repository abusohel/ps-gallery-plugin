<!--  loop Starts -->
<?php

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <div class="item">
        <?php ( !empty($ps_select_theme) ) ?
                include PS_PLUGIN_DIR .'/themes/'.$ps_select_theme .'.php'
                :
                include PS_PLUGIN_DIR .'/themes/gallerydefault.php';
        ?>

    </div>
<?php endwhile; ?>
<!--Loop Ends-->
</div> <!-- ends ps-gallery-block-wrapper -->








