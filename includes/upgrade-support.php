<?php

/**
 * Enables shortcode for Widget
 */

add_filter('widget_text', 'do_shortcode');

/*
 * Load Text domain after plugin has been loaded
 * */

add_action('plugins_loaded', 'pxlr_load_textdomain');
function pxlr_load_textdomain(){
    load_plugin_textdomain(PS_TEXTDOMAIN, false, plugin_basename( dirname( __FILE__ ) ) . '/languages/');
}