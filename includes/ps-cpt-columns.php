<?php

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

function ps_add_new_columns($new_columns){
    $new_columns = [];
    $new_columns['cb']   = '<input type="checkbox" />';
    $new_columns['title']   = __('Gallery Block Name', PS_TEXTDOMAIN);
    $new_columns['shortcode']   = __('Gallery Block Shortcode', PS_TEXTDOMAIN);
    $new_columns['shortcode_2']   = __('Shortcode For Template File', PS_TEXTDOMAIN);
    $new_columns['date']   = __('Created at', PS_TEXTDOMAIN);
    return $new_columns;
}
add_filter('manage_psgalleryblock_posts_columns', 'ps_add_new_columns');

function ps_manage_custom_columns( $column_name, $post_id ) {

    switch($column_name){
        case 'shortcode': ?>
            <textarea style="resize: none; background-color: #2e8498; color: #fff;" cols="26" rows="1" onClick="this.select();" >[ps-gallery-block <?php echo 'id="'.$post_id.'"';?>]</textarea>
        <?php
        break;
        case 'shortcode_2':
            ?>
                    <textarea style="resize: none; background-color: #259888; color: #fff;" cols="65" rows="1" onClick="this.select();" ><?php echo '<?php echo do_shortcode("[ps-hero-block id='; echo "'".$post_id."']"; echo '"); ?>'; ?></textarea>
            <?php
            break;

        default:
            break;

    }
}

add_action('manage_psgalleryblock_posts_custom_column', 'ps_manage_custom_columns', 10, 2);



