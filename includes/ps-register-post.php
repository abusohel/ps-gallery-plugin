<?php

if ( ! defined( 'ABSPATH' ) ) die( PS_ALERT_MSG );

function ps_init() {
    $post_type = 'psgalleryblock';
    $singular_name = 'Ps Gallery Block';
    $plural_name = 'Ps Gallery Blocks';
    $slug = 'psgalleryblock';
    $labels = array(
        'name'               => _x( $plural_name, 'post type general name', PS_TEXTDOMAIN ),
        'singular_name'      => _x( $singular_name, 'post type singular name', PS_TEXTDOMAIN ),
        'menu_name'          => _x( $singular_name, 'admin menu name', PS_TEXTDOMAIN ),
        'name_admin_bar'     => _x( $singular_name, 'add new name on admin bar', PS_TEXTDOMAIN ),
        'add_new'            => _x( 'Add New', 'add new text', PS_TEXTDOMAIN ),
        'add_new_item'       => __( 'Add New '.$singular_name, PS_TEXTDOMAIN ),
        'new_item'           => __( 'New '.$singular_name, PS_TEXTDOMAIN ),
        'edit_item'          => __( 'Edit '.$singular_name, PS_TEXTDOMAIN ),
        'view_item'          => __( 'View '.$singular_name, PS_TEXTDOMAIN ),
        'all_items'          => __( 'All '.$plural_name, PS_TEXTDOMAIN ),
        'search_items'       => __( 'Search '.$plural_name, PS_TEXTDOMAIN ),
        'parent_item_colon'  => __( 'Parent '.$plural_name.':', PS_TEXTDOMAIN ),
        'not_found'          => __( 'No sliders found.', PS_TEXTDOMAIN ),
        'not_found_in_trash' => __( 'No books found in Trash.', PS_TEXTDOMAIN )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', PS_TEXTDOMAIN ),
        'public'             => false,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => $slug ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'           => 'dashicons-grid-view'


    );

    register_post_type( $post_type, $args );
}

add_action( 'init', 'ps_init');



/**
 * Change the placeholder of title input box
 * @param string $title Name of the book
 *
 * @return string
 */
function ps_change_title_text( $title ){
    $screen = get_current_screen();
    if  ( 'psgalleryblock' == $screen->post_type ) {
        $title = 'Enter a slider title';
    }
    return $title;
}

add_filter( 'enter_title_here', 'ps_change_title_text' );